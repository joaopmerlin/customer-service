package br.com.joaomerlin.customerservice.controller;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.joaomerlin.customerservice.entity.Customer;
import br.com.joaomerlin.customerservice.service.CustomerService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RestController
@RequestMapping("customer")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CustomerController {

    CustomerService service;

    @GetMapping("{id}")
    public Customer findByID(@PathVariable UUID id) {
        return service.findById(id);
    }

    @GetMapping
    public Page<Customer> findAll(@RequestParam(required = false, defaultValue = "0") int page,
                                  @RequestParam(required = false, defaultValue = "10") int size,
                                  @RequestParam(required = false, defaultValue = "") String name,
                                  @RequestParam(required = false, defaultValue = "") String cpf) {
        return service.findAll(name, cpf, PageRequest.of(page, size));
    }

    @PostMapping
    public Customer create(@RequestBody Customer customer) {
        return service.create(customer);
    }

    @PutMapping("{id}")
    public Customer update(@PathVariable UUID id, @RequestBody Customer customer) {
        return service.update(id, customer);
    }

    @PatchMapping("{id}")
    public void patch(@PathVariable UUID id, @RequestBody Customer customer) {
        service.patch(id, customer);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable UUID id) {
        service.delete(id);
    }
}
