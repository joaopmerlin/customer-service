package br.com.joaomerlin.customerservice.entity;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.joaomerlin.customerservice.util.CpfValidator;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Customer {

    @Id
    @GeneratedValue
    UUID id;

    String name;

    String cpf;

    @Column(name = "birth_date")
    LocalDate birthDate;

    @Transient
    Integer age;

    public Customer(UUID id, String name, String cpf, LocalDate birthDate) {
        this.id = id;
        this.name = name;
        this.cpf = cpf;
        this.birthDate = birthDate;
    }

    public Integer getAge() {
        return birthDate != null ? (int) ChronoUnit.YEARS.between(birthDate, LocalDate.now()) : null;
    }

    @JsonIgnore
    public boolean isValid() {
        return StringUtils.isNotBlank(name) && CpfValidator.isCPF(cpf) && birthDate != null;
    }
}
