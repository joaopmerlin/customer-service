package br.com.joaomerlin.customerservice.service;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.joaomerlin.customerservice.entity.Customer;

public interface CustomerService {

    Customer findById(UUID id);

    Page<Customer> findAll(String name, String cpf, Pageable pageable);

    Customer create(Customer customer);

    Customer update(UUID id, Customer customer);

    Customer patch(UUID id, Customer customer);

    void delete(UUID id);
}
