package br.com.joaomerlin.customerservice.service;

import static br.com.joaomerlin.customerservice.entity.QCustomer.customer;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import com.querydsl.core.types.dsl.BooleanExpression;

import br.com.joaomerlin.customerservice.entity.Customer;
import br.com.joaomerlin.customerservice.repository.CustomerRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@Lazy
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CustomerServiceImpl implements CustomerService {

    CustomerRepository repository;

    @Override
    @Transactional(readOnly = true)
    public Customer findById(UUID id) {
        return repository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Customer> findAll(String name, String cpf, Pageable pageable) {
        BooleanExpression expression = customer.cpf.likeIgnoreCase(getLikeParam(cpf))
                .and(customer.name.likeIgnoreCase(getLikeParam(name)));
        return repository.findAll(expression, pageable);
    }

    @Override
    @Transactional
    public Customer create(Customer customer) {
        if (customer.getId() != null || !customer.isValid()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        if (repository.findByCpf(customer.getCpf()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
        return repository.save(customer);
    }

    @Override
    @Transactional
    public Customer update(UUID id, Customer customer) {
        if (!customer.isValid()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        customer.setId(id);
        return repository.save(customer);
    }

    @Override
    @Transactional
    public Customer patch(UUID id, Customer customer) {
        Customer toSave = findById(id);
        if (StringUtils.isNotBlank(customer.getName())) {
            toSave.setName(customer.getName());
        }
        if (StringUtils.isNotBlank(customer.getCpf())) {
            toSave.setCpf(customer.getCpf());
        }
        if (customer.getBirthDate() != null) {
            toSave.setBirthDate(customer.getBirthDate());
        }
        return repository.save(toSave);
    }

    @Override
    @Transactional
    public void delete(UUID id) {
        repository.delete(findById(id));
    }

    private String getLikeParam(String value) {
        return value == null ? "%%" : '%' + value + '%';
    }
}
