package br.com.joaomerlin.customerservice.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import br.com.joaomerlin.customerservice.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, UUID>, QuerydslPredicateExecutor<Customer> {

    Optional<Customer> findByCpf(String cpf);
}
