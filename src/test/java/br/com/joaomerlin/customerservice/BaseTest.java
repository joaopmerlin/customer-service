package br.com.joaomerlin.customerservice;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.h2.tools.RunScript;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import liquibase.exception.LiquibaseException;
import liquibase.integration.spring.SpringLiquibase;

@SpringBootTest
@AutoConfigureMockMvc
public abstract class BaseTest {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SpringLiquibase liquibase;

    @BeforeEach
    public void runScripts() throws SQLException, IOException, LiquibaseException {
        try (Connection conn = dataSource.getConnection()) {
            resetDatabase(conn);
            String scriptFile = "scripts/" + this.getClass().getSimpleName() + ".sql";
            InputStream scriptResource = ClassLoader.getSystemResourceAsStream(scriptFile);
            if (scriptResource != null) {
                try (InputStreamReader isr = new InputStreamReader(scriptResource)) {
                    RunScript.execute(conn, isr);
                }
            }
        }
    }

    private void resetDatabase(Connection conn) throws SQLException, LiquibaseException {
        try (Statement statement = conn.createStatement()) {
            statement.execute("DROP ALL OBJECTS");
        }
        liquibase.afterPropertiesSet();
    }

}
