package br.com.joaomerlin.customerservice.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import br.com.joaomerlin.customerservice.BaseTest;

public class CpfValidatorTest extends BaseTest {

    @Test
    public void validCpfTest() {
        assertThat(CpfValidator.isCPF("08046777943")).isTrue();
    }

    @Test
    public void invalidCpfTest() {
        assertThat(CpfValidator.isCPF("12345678910")).isFalse();
    }
}
