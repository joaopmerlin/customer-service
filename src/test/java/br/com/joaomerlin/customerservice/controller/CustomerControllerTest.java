package br.com.joaomerlin.customerservice.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.joaomerlin.customerservice.BaseTest;
import br.com.joaomerlin.customerservice.entity.Customer;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class CustomerControllerTest extends BaseTest {

    @Autowired
    MockMvc mvc;

    @Autowired
    ObjectMapper mapper;

    @Test
    public void validationTest() throws Exception {
        Customer customer = new Customer();
        customer.setName("João Paulo Merlin");
        customer.setCpf("08046777943");

        mvc.perform(post("/customer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(customer)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void conflictTest() throws Exception {
        Customer customer = new Customer();
        customer.setName("João");
        customer.setCpf("58665433031");
        customer.setBirthDate(LocalDate.of(1994, 1, 24));

        mvc.perform(post("/customer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(customer)))
                .andExpect(status().isConflict());
    }

    @Test
    public void createTest() throws Exception {
        Customer customer = new Customer();
        customer.setName("João Paulo Merlin");
        customer.setCpf("08046777943");
        customer.setBirthDate(LocalDate.of(1994, 1, 24));

        mvc.perform(post("/customer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(customer)))
                .andExpect(status().isOk());
    }

    @Test
    public void updateTest() throws Exception {
        Customer customer = new Customer();
        customer.setName("João");
        customer.setCpf("58665433031");
        customer.setBirthDate(LocalDate.of(1990, 1, 2));

        mvc.perform(put("/customer/{id}", "46cd84c3-4f2b-4b58-9165-d71456574a21")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(customer)))
                .andExpect(status().isOk());
    }

    @Test
    public void patchTest() throws Exception {
        Customer customer = new Customer();
        customer.setName("João Paulo");

        mvc.perform(patch("/customer/{id}", "46cd84c3-4f2b-4b58-9165-d71456574a21")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(customer)))
                .andExpect(status().isOk());
    }

    @Test
    public void notFoundTest() throws Exception {
        mvc.perform(get("/customer/{id}", "46cd84c3-4f2b-4b58-9165-d71456574a29")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void findByIdTest() throws Exception {
        mvc.perform(get("/customer/{id}", "46cd84c3-4f2b-4b58-9165-d71456574a21")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void findAllTest() throws Exception {
        mvc.perform(get("/customer")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void findAllPageTest() throws Exception {
        mvc.perform(get("/customer?page={page}&size={size}", 0, 10)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void findAllFilterTest() throws Exception {
        mvc.perform(get("/customer?name={name}&cpf={cpf}", "João", "58665433031")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteTest() throws Exception {
        mvc.perform(delete("/customer/{id}", "46cd84c3-4f2b-4b58-9165-d71456574a21")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
