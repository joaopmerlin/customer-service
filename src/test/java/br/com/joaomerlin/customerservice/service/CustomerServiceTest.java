package br.com.joaomerlin.customerservice.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.time.LocalDate;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.server.ResponseStatusException;

import br.com.joaomerlin.customerservice.BaseTest;
import br.com.joaomerlin.customerservice.entity.Customer;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class CustomerServiceTest extends BaseTest {

    @Autowired
    CustomerService service;

    @Test
    public void validationTest() {
        Customer customer = new Customer();
        customer.setName("João Paulo Merlin");
        customer.setCpf("08046777943");

        assertThatThrownBy(() -> service.create(customer))
                .isInstanceOf(ResponseStatusException.class)
                .hasMessage("400 BAD_REQUEST");
    }

    @Test
    public void conflictTest() {
        Customer customer = new Customer();
        customer.setName("João");
        customer.setCpf("58665433031");
        customer.setBirthDate(LocalDate.of(1994, 1, 24));

        assertThatThrownBy(() -> service.create(customer))
                .isInstanceOf(ResponseStatusException.class)
                .hasMessage("409 CONFLICT");
    }

    @Test
    public void createTest() {
        Customer customer = new Customer();
        customer.setName("João Paulo Merlin");
        customer.setCpf("08046777943");
        customer.setBirthDate(LocalDate.of(1994, 1, 24));

        Customer saved = service.create(customer);

        assertThat(saved).isEqualToIgnoringGivenFields(customer, "id");
    }

    @Test
    public void updateTest() {
        UUID id = UUID.fromString("46cd84c3-4f2b-4b58-9165-d71456574a21");

        Customer customer = new Customer();
        customer.setId(id);
        customer.setName("João");
        customer.setCpf("58665433031");
        customer.setBirthDate(LocalDate.of(1990, 1, 2));

        Customer saved = service.update(id, customer);

        assertThat(saved).isEqualTo(customer);
    }

    @Test
    public void patchTest() {
        UUID id = UUID.fromString("46cd84c3-4f2b-4b58-9165-d71456574a21");

        Customer customer = new Customer();
        customer.setName("João Paulo");

        Customer saved = service.patch(id, customer);

        Customer expected = new Customer();
        expected.setId(UUID.fromString("46cd84c3-4f2b-4b58-9165-d71456574a21"));
        expected.setName("João Paulo");
        expected.setCpf("58665433031");
        expected.setBirthDate(LocalDate.of(1990, 1, 1));

        assertThat(saved).isEqualTo(expected);
    }

    @Test
    public void findByIdNotFoundTest() {
        assertThatThrownBy(() -> service.findById(UUID.fromString("46cd84c3-4f2b-4b58-9165-d71456574a20")))
                .isInstanceOf(ResponseStatusException.class)
                .hasMessage("404 NOT_FOUND");
    }

    @Test
    public void findByIdTest() {
        Customer customer = service.findById(UUID.fromString("46cd84c3-4f2b-4b58-9165-d71456574a23"));

        Customer expected = new Customer();
        expected.setId(UUID.fromString("46cd84c3-4f2b-4b58-9165-d71456574a23"));
        expected.setName("José");
        expected.setCpf("53843393060");
        expected.setBirthDate(LocalDate.of(1992, 1, 1));

        assertThat(customer).isEqualTo(expected);
    }

    @Test
    public void findAllTest() {
        Page<Customer> all = service.findAll(null, null, PageRequest.of(0, 10));

        assertThat(all.getTotalElements()).isEqualTo(4);
        assertThat(all.getContent()).containsExactlyInAnyOrder(
                new Customer(UUID.fromString("46cd84c3-4f2b-4b58-9165-d71456574a21"), "João", "58665433031", LocalDate.of(1990, 1, 1)),
                new Customer(UUID.fromString("46cd84c3-4f2b-4b58-9165-d71456574a22"), "Paulo", "27239859037", LocalDate.of(1991, 1, 1)),
                new Customer(UUID.fromString("46cd84c3-4f2b-4b58-9165-d71456574a23"), "José", "53843393060", LocalDate.of(1992, 1, 1)),
                new Customer(UUID.fromString("46cd84c3-4f2b-4b58-9165-d71456574a24"), "Maria", "07044792065", LocalDate.of(1993, 1, 1))
        );
    }

    @Test
    public void findAllFilterNameTest() {
        Page<Customer> all = service.findAll("João", null, PageRequest.of(0, 10));

        assertThat(all.getTotalElements()).isEqualTo(1);
        assertThat(all.getContent()).containsExactlyInAnyOrder(
                new Customer(UUID.fromString("46cd84c3-4f2b-4b58-9165-d71456574a21"), "João", "58665433031", LocalDate.of(1990, 1, 1))
        );
    }

    @Test
    public void findAllFilterCpfTest() {
        Page<Customer> all = service.findAll(null, "58665433031", PageRequest.of(0, 10));

        assertThat(all.getTotalElements()).isEqualTo(1);
        assertThat(all.getContent()).containsExactlyInAnyOrder(
                new Customer(UUID.fromString("46cd84c3-4f2b-4b58-9165-d71456574a21"), "João", "58665433031", LocalDate.of(1990, 1, 1))
        );
    }

    @Test
    public void findAllPageTest() {
        Page<Customer> all = service.findAll(null, null, PageRequest.of(0, 2));

        assertThat(all.getTotalElements()).isEqualTo(4);
        assertThat(all.getNumberOfElements()).isEqualTo(2);
        assertThat(all.getContent()).containsExactlyInAnyOrder(
                new Customer(UUID.fromString("46cd84c3-4f2b-4b58-9165-d71456574a21"), "João", "58665433031", LocalDate.of(1990, 1, 1)),
                new Customer(UUID.fromString("46cd84c3-4f2b-4b58-9165-d71456574a22"), "Paulo", "27239859037", LocalDate.of(1991, 1, 1))
        );
    }

    @Test
    public void deleteTest() {
        service.delete(UUID.fromString("46cd84c3-4f2b-4b58-9165-d71456574a21"));

        assertThatThrownBy(() -> service.findById(UUID.fromString("46cd84c3-4f2b-4b58-9165-d71456574a21")))
                .isInstanceOf(ResponseStatusException.class)
                .hasMessage("404 NOT_FOUND");
    }

}
